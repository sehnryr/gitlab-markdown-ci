---
title: Gitlab Markdown CI Setup
---

This repository is a template / demo for my markdown CI setup. It functionally
mirrors the [setup I have in this 
gist](https://gist.github.com/MyriaCore/75729707404cba1c0de89cc03b7a6adf),
with the exception that it runs on Gitlab CI/CD to produce a pages instance
out of all of the markdown documents in the repository. 


Because of gitlab-org/gitlab#243533, notes that use katex in gitlab markdown 
are kinda buggy. Along with issues like gitlab-org/gitlab#215084, it makes sense
to find a nice workaround for notes that make use of this. 

The goal is to support common gitlab markdown features that I use in my notes.
This includes, but isn't limited to:

- PlantUML and Mermaid graphs
- katex math
- linking to files in the repository

Here are some features that I'm planning on adding support for:

- [Rouge's supported languages for syntax highlighting](http://rouge.jneen.net/)
  * <http://www.frommknecht.net/pandoc-github-highlighting>
- [Special Gitlab References](https://docs.gitlab.com/ee/user/markdown.html#special-gitlab-references)
- [Emoji](https://docs.gitlab.com/ee/user/markdown.html#emoji)
- [Task Lists](https://docs.gitlab.com/ee/user/markdown.html#task-lists)
- [Inline Diff](https://docs.gitlab.com/ee/user/markdown.html#inline-diff)
- [Color swatches](https://docs.gitlab.com/ee/user/markdown.html#colors) e.g. `#FF00FF`
- Copying text in the html will copy markdown

Below are some test snippets to ensure these features are working. This document
can be viewed in the [pages instance](https://myriacore.gitlab.io/gitlab-markdown-ci/README.html).

I am building trypandoc-style app [here](https://gitlab.com/myriacore/trypandoc-cli). You can use that to test this setup
out for yourself. 

```math
{{m+n} \choose m } = \frac{(m+n)!}{m!n!}
```

```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```

```ditaa
+--------+   +-------+    +-------+
|        +---+ ditaa +--> |       |
|  Text  |   +-------+    |diagram|
|Document|   |!magic!|    |       |
|     {d}|   |       |    |       |
+---+----+   +-------+    +-------+
    :                         ^
    |       Lots of work      |
    +-------------------------+
```

## Emoji test

Sometimes you want to :monkey: around a bit and add some :star2: to your :speech_balloon:. Well we have a gift for you:

:zap: You can use emoji anywhere GitLab Flavored Markdown is supported. :v:

You can use it to point out a :bug: or warn about :speak_no_evil: patches. And if someone improves your really :snail: code, send them some :birthday:. People :heart: you for that.

If you're new to this, don't be :fearful:. You can join the emoji :family:. All you need to do is to look up one of the supported codes.

Consult the [Emoji Cheat Sheet](https://www.emojicopy.com) for a list of all supported emoji codes. :thumbsup:
